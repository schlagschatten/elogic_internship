define([
    'jquery',
    'Magento_Customer/js/customer-data'
],function ($, customerData) {
    'use strict';

    let mixin = {
        initialize: function () {
            this._super();
            this.initRegistries();
        },

        initRegistries: function() {
            var customerRegistries = customerData.get('gift-registry')();
            var customer = customerData.get('customer');

            customer.subscribe(function (updatedCustomer) {
                this.isLoggedIn(updatedCustomer.firstname || false);
            }, this);

            this.registries(customerRegistries.registries || []);
            this.selected(customerRegistries.selected || []);
            this.hasRegistries(this.registries().length > 0);
        },

        defineBehaviour: function(data, event) {
            this.initRegistries();

            if (this.registries().length == 1) {
                event.stopPropagation();
                this.addProduct();
            }

            //if user is not logged in, we open login page with url
            // to be able return back to the product page
            if (!this.isLoggedIn()) {
                window.location.href = this.loginUrl;
            }
        },

        addProduct: function() {
            if (!$('#product_addtocart_form').valid()) {
                return false;
            }

            $.ajax({
                url: this.url,
                method: 'POST',
                data: this.getData(),
                dataType: 'json',
                showLoader: true,
                success: function (response) {
                    $('.giftr-dropdown').hide();
                    if (response.status === this.login) {
                        setLocation(response.message);
                    }
                }
            });
        }

    };

    return function (target) {
        return target.extend(mixin);
    };
});
