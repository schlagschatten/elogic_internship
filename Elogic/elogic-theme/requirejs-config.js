var config = {
    paths: {
        PopupGiftr:  'js/popup-giftr',
        'JQueryUi': 'js/jquery-ui'
    },
    config: {
        mixins: {
            'Mirasvit_Giftr/js/item': {
                'js/item-mixin': true
            }
        }
    },
    shim: {
        slick: {
            deps: ['jquery']
        }
    }
}
